# Porkchop Master Race

## The Summary

This data pack for Minecraft: Java Edition (and behavior pack for Bedrock) makes pigs the only passive land mob that drops meat, thus making them useful and no longer redundant.

## The Story

Long ago, every passive mob had a unique purpose. None were redundant. Chickens dropped feathers. Cows dropped leather. Sheep dropped wool. Pigs held the honorable position of being the one and only passive mob that dropped meat.

But all that changed with the Adventure Update. Beta 1.8 introduced meat drops for cows and chickens, and then 1.8 (the Bountiful Update) introduced mutton and rabbit meat. This left pigs in a weird spot. All the meat-dropping land animals have additional drops and functionality, but pigs have nothing except a riding gimmick that has always been practically useless. Pigs, despite being an iconic part of the game (and serving as the basis for what we now call piglins), have become totally redundant.

There are two ways to fix this problem that I can think of: add new functionality to pigs, or remove the meat drops from the other mobs. This data pack does the latter.

With this pack installed, you can restore the long-lost balance to the world of passive mobs. It's time to MAKE PIGS GREAT AGAIN!

## The Details

### Changes in both versions

Removed meat drops from chickens, cows, mooshrooms, rabbits and sheep. Pigs are now the only passive, non-aquatic mob that drops food, thus making them useful again.

To prevent the other passive mobs from sometimes dropping nothing, their loot tables have been adjusted:

Raised minimum feather drop count for chickens from 0 to 1.
Raised minimum leather drop count for cows/mooshrooms from 0 to 1.
Raised minimum rabbit hide drop count for rabbits from 0 to 1. (Great for making bundles a bit easier to obtain in the early game!)

Chest loot tables have been altered such that all entries for the "removed" meats have been merged/replaced with porkchops. The meat versus non-meat item ratios have been kept intact.

Instead of cats gifting raw chicken to players, they will now gift raw cod. (Changing it to porkchops wouldn't make sense since cats don't hunt pigs.)

Butcher villager trades have been adjusted to account for the lack of other meats to buy/sell. All the non-porkchop meat trades are gone (as well as trades involving rabbit stew), and the buy-porkchops trade has been moved from apprentice to journeyman level (because otherwise there wouldn't be anything in that level).

For storage convenience, crafting recipes have been added to allow you to convert your existing beef/mutton/etc. into porkchops.

### Java-only changes

The "Hero of the Village" butcher gift loot table has been adjusted to remove non-pork meats. (Bedrock doesn't have the gift mechanic yet, so there's nothing to change there.)

The "removed" meats (+ rabbit stew, for obvious reasons) are no longer required for the "Balanced Diet" challenge advancement.

The data pack will alter the trades of novice villagers, but not those that have already leveled up. (I'm actually not sure what the behavior is for the Bedrock pack. I assume that pre-existing villagers are not changed.)

Special thanks to BlockyGoddess for https://www.planetminecraft.com/data-pack/custom-villager-trades-customize-your-villagers-modders-tool/, which was the basis for the trade-altering code in the Java Edition pack.

## The (Un)licensing

This is free and unencumbered software released into the public domain. See UNLICENSE.txt for more details.
